function moveNav(){
	if(document.getElementById("sideButton").style.visibility == "hidden"){
		document.getElementById("sideBar").style.visibility = "hidden";
		document.getElementById("mainContainer").style.width = "100%";
		document.getElementById("mainContainer").style.marginLeft = "0%";
		document.getElementById("topNav").style.marginLeft = "0%";
		document.getElementById("sideButton").style.visibility = "visible";
	}
	else{
		document.getElementById("sideBar").style.visibility = "visible";
		document.getElementById("sideBar").style.width = "15%";
		document.getElementById("mainContainer").style.width = "85%";
		document.getElementById("mainContainer").style.marginLeft = "15%";
		document.getElementById("topNav").style.marginLeft = "15%";
		document.getElementById("sideButton").style.visibility = "hidden";
	}	
}